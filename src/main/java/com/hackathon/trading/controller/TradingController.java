package com.hackathon.trading.controller;

import com.hackathon.trading.entities.Trading;
import com.hackathon.trading.service.TradingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;



import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/v1/trading/")
@CrossOrigin("*")
public class TradingController{

    private static final Logger LOG = LoggerFactory.getLogger(TradingController.class);

    @Autowired
    private TradingService tradingService;
    @GetMapping
    public ResponseEntity<List<Trading>> findAll(){
        try {
            return new ResponseEntity<List<Trading>>(tradingService.findAll(), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("{id}")
    public ResponseEntity<Trading> findById(@PathVariable int id) {
        try {
            return new ResponseEntity<Trading>(tradingService.findById(id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }


    @PostMapping
    public ResponseEntity<Trading> save(@RequestBody Trading trading) {
        try {
            trading.setId(0);
            LOG.debug("Request to create trading [" + trading + "]");
            return new ResponseEntity<Trading>(tradingService.save(trading), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping
    public ResponseEntity<Trading>update(@RequestBody Trading trading) {
        try {
            return new ResponseEntity<Trading>(tradingService.update(trading), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            LOG.debug("update for unknown id: [" + trading + "]");
            return new ResponseEntity<Trading>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        try {
            tradingService.delete(id);
        } catch (EmptyResultDataAccessException ex)

        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}/{stockTicker}")
    public ResponseEntity<List<Trading>> findByStockTicker(@PathVariable String stockTicker) {
        try {
            return new ResponseEntity<>(tradingService.findByStockTicker(stockTicker), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }


}



