package com.hackathon.trading.repository;

import com.hackathon.trading.entities.Trading;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TradingRepository extends JpaRepository<Trading, Integer> {

    List<Trading> findTradingByStockTicker(String stockTicker);
}
