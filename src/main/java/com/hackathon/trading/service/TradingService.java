package com.hackathon.trading.service;

import com.hackathon.trading.entities.Trading;
import com.hackathon.trading.repository.TradingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class TradingService {
    @Autowired
    TradingRepository tradingRepository;

    public List<Trading> findAll(){return tradingRepository.findAll();}

    public Trading findById(int id){
        return tradingRepository.findById(id).get();
    }


    //Create a record
    public Trading save(Trading trading) {
        return this.tradingRepository.save(trading);
    }

    public void delete(int id){tradingRepository.deleteById(id);}

    //Edit a record
    public Trading update(Trading trading) {
        tradingRepository.findById(trading.getId()).get();

        return tradingRepository.save(trading);
    }

    public  List<Trading> findByStockTicker(String stockTicker){
        return tradingRepository.findTradingByStockTicker(stockTicker);
    }

}



