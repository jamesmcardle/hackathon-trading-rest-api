CREATE TABLE trading
(id INT NOT NULL AUTO_INCREMENT,
 buyOrSell VARCHAR(40),
 price INT ,
 statusCode INT,
 stockTicker VARCHAR(24),
 volume INT,
createdTimestamp DATETIME,
 PRIMARY KEY(id));

INSERT INTO trading (id, buyOrSell,price,statusCode,stockTicker, volume,createdTimestamp)  VALUES (1, 'buy', 12,1,'lol',11,'2021-10-08');
INSERT INTO trading (id, buyOrSell,price,statusCode,stockTicker, volume,createdTimestamp)  VALUES (2, 'sell', 212,3,'AMZN',171,'2021-12-01');
