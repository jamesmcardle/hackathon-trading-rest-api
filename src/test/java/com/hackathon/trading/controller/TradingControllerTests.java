package com.hackathon.trading.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.hackathon.trading.entities.Trading;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class TradingControllerTests {
    private final String baseRestUrl="/api/v1/trading/";
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testFindAll() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get(baseRestUrl))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        List<Trading> tradings=new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(), new TypeReference<List<Trading>>() {
                });
        assertThat(tradings.size()).isGreaterThan(1);
    }


    @Test
    public void testFindAllNotFound() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/trade/"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }



    @Test
    public void testFindById()throws Exception{
        int testId=1;
        MvcResult mvcResult = this.mockMvc.perform(get(baseRestUrl+testId))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        Trading tradings=new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),new TypeReference<Trading>() {
                });
        assertThat(tradings.getId()).isEqualTo(testId);
    }

    @Test
    public void testFindByIdNotFound() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get(baseRestUrl+99))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void testFindByStock()throws Exception{
        String testStock="AMZN";

        MvcResult mvcResult = this.mockMvc.perform(get(baseRestUrl+1+"/"+testStock))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        List<Trading> tradings=new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(), new TypeReference<List<Trading>>() {
                });

        assertThat(tradings.get(0).getStockTicker()).isEqualTo(testStock);
    }


    @Test
    public void testCreate()throws Exception{
        int testId=1;
        Date testCreatedTimestamp = new Date(System.currentTimeMillis());
        String testBuyOrSell="buy";
        String testStockTicker = "AMZN";
        double testPrice = 10.0;
        int testVolume = 1000;
        int testStatusCode = 1;

        Trading trade=new Trading();
        trade.setId(testId);
        trade.setPrice(testPrice);
        trade.setVolume(testVolume);
        trade.setStatusCode(testStatusCode);
        trade.setBuyOrSell(testBuyOrSell);
        trade.setCreatedTimestamp(testCreatedTimestamp);
        trade.setStockTicker(testStockTicker);

        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(trade);

        MvcResult mvcResult = this.mockMvc.perform(post(baseRestUrl)
                .header("Content-Type", "application/json")
                .content(requestJson))
                .andDo(print()).andExpect(status().isOk()).andReturn();

        Trading trading=new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<Trading>() { });

        assertThat(trading.getId()).isGreaterThan(0);
        assertThat(trading.getPrice()).isEqualTo(testPrice);
        assertThat(trading.getStatusCode()).isEqualTo(testStatusCode);
        assertThat(trading.getVolume()).isEqualTo(testVolume);
        assertThat(trading.getCreatedTimestamp()).isEqualTo(testCreatedTimestamp);
        assertThat(trading.getStockTicker()).isEqualTo(testStockTicker);
        assertThat(trading.getBuyOrSell()).isEqualTo(testBuyOrSell);
    }

    @Test
    public void testDelete()throws Exception{
        int testId=1;
        MvcResult mvcResult = this.mockMvc.perform(delete(baseRestUrl + testId)
                .header("Content-Type", "application/json"))
                .andDo(print()).andExpect(status().isNoContent()).andReturn();


        this.mockMvc.perform(get(baseRestUrl + testId))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
    @Test
    public void testEdit()throws Exception{
        int testId=1;
        Date testCreatedTimestamp = new Date(System.currentTimeMillis());
        String testBuyOrSell="buy";
        String testStockTicker = "AMZN";
        double testPrice = 10.0;
        int testVolume = 1000;
        int testStatusCode = 1;

        Trading trade=new Trading();
        trade.setId(testId);
        trade.setPrice(testPrice);
        trade.setVolume(testVolume);
        trade.setStatusCode(testStatusCode);
        trade.setBuyOrSell(testBuyOrSell);
        trade.setCreatedTimestamp(testCreatedTimestamp);
        trade.setStockTicker(testStockTicker);

        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(trade);

        MvcResult mvcResult = this.mockMvc.perform(put(baseRestUrl)
                .header("Content-Type", "application/json")
                .content(requestJson))
                .andDo(print()).andExpect(status().isOk()).andReturn();

        Trading trading=new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<Trading>() { });

        assertThat(trading.getId()).isGreaterThan(0);
        assertThat(trading.getPrice()).isEqualTo(testPrice);
        assertThat(trading.getStatusCode()).isEqualTo(testStatusCode);
        assertThat(trading.getVolume()).isEqualTo(testVolume);
        assertThat(trading.getCreatedTimestamp()).isEqualTo(testCreatedTimestamp);
        assertThat(trading.getStockTicker()).isEqualTo(testStockTicker);
        assertThat(trading.getBuyOrSell()).isEqualTo(testBuyOrSell);

        mvcResult = this.mockMvc.perform(get(baseRestUrl +testId))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();


        trading = new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<Trading>() { });

        assertThat(trading.getId()).isGreaterThan(0);
        assertThat(trading.getPrice()).isEqualTo(testPrice);
        assertThat(trading.getStatusCode()).isEqualTo(testStatusCode);
        assertThat(trading.getVolume()).isEqualTo(testVolume);
        assertThat(trading.getCreatedTimestamp()).isEqualTo(testCreatedTimestamp);
        assertThat(trading.getStockTicker()).isEqualTo(testStockTicker);
        assertThat(trading.getBuyOrSell()).isEqualTo(testBuyOrSell);

    }
}
