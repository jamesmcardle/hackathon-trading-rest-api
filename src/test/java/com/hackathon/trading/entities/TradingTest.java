package com.hackathon.trading.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TradingTest {

    private Trading trading;
    private static final String testStockTicker = "AMZN";
    private static final double testPrice = 10.0;
    private static final int testVolume = 1000;
    private static final int testStatusCode = 1;

    @BeforeEach
    public void setup() {
        this.trading = new Trading();
    }

    @Test
    public void setPriceTest() {
        this.trading.setPrice(testPrice);
        assertEquals(this.trading.getPrice(), testPrice);
    }

    @Test
    public void getPriceTest(){
        this.trading.setPrice(testPrice);
        assertEquals(this.trading.getPrice(),10.0);
    }

    @Test
    public void setVolumeTest() {
        this.trading.setVolume(testVolume);
        assertEquals(this.trading.getVolume(), testVolume);
    }

    @Test
    public void setStatusCodeTest() {
        this.trading.setStatusCode(testStatusCode);
        assertEquals(this.trading.getStatusCode(), testStatusCode);
    }

}
